#import "AttachmentView.h"

@implementation AttachmentView

- (id)init {
    self = [super init];
    if (self) {
        [self addGravity];
    }
    return self;
}

- (void)addGravity {
    theAnimator = [[UIDynamicAnimator alloc] initWithReferenceView:self];
    UIGravityBehavior *gravityBehaviour = [[UIGravityBehavior alloc] initWithItems:@[ballView]];
    [theAnimator addBehavior:gravityBehaviour];
    CGPoint point = CGPointMake(160.0, 300.0);
    UIAttachmentBehavior *attachment;
    attachment = [[UIAttachmentBehavior alloc] initWithItem:ballView attachedToAnchor:point];
    [attachment setLength:100];
    [attachment setFrequency:50];
    [attachment setDamping:5];
    [theAnimator addBehavior:attachment];
    [gravityBehaviour addItem:ballView];

}
@end